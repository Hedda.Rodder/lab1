package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    String playerChoice;


    public static void main(String[] args) {
        /*
         * The code here does two things:
         * It first creates a new RockPaperScissors -object with the
         * code `new RockPaperScissors()`. Then it calls the `run()`
         * method on the newly created object.
         */
        new RockPaperScissors().run();
    }

    Scanner sc = new Scanner(System.in);

    static boolean isWinner(String playerChoice, String computerChoice) {
        if (playerChoice.equals("rock")) {
            return computerChoice.equals("scissors");
        } else if (playerChoice.equals("paper")) {
            return computerChoice.equals("rock");
        } else if (playerChoice.equals("scissors")) {
            return computerChoice.equals("paper");
        }
        else {
            return false;
        }
    }


    public void run() {
        // List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");

        while (true) {
            System.out.println("Let's play round " + roundCounter);

            while (true) {
                playerChoice = readInput("Your choice (Rock/Paper/Scissors)?");
                if (playerChoice.equals("rock") || playerChoice.equals("paper") || playerChoice.equals("scissors")) {
                    break;
                }
                System.out.println("I do not understand " + playerChoice + ". Could you try again?");
                continue;
            }

            Random random = new Random();
            int randomNumber = random.nextInt(3);

            String computerChoice;
            {
                if (randomNumber == 0) {
                    computerChoice = "rock";
                } else if (randomNumber == 1) {
                    computerChoice = "paper";
                } else { // if (randomNumber == 2) {
                    computerChoice = "scissors";
                }
            }

            if (playerChoice.equals(computerChoice)) {
                System.out.println("Human chose " + playerChoice + ", " + "computer chose " + computerChoice + ". It's a tie!");
            } else if (isWinner(playerChoice, computerChoice)) {
                System.out.println("Human chose " + playerChoice + ", " + "computer chose " + computerChoice + ". Player wins!");
                humanScore++;
            } else if (isWinner(computerChoice, playerChoice)) {
                System.out.println("Human chose " + playerChoice + ", " + "computer chose " + computerChoice + ". Computer wins!");
                computerScore++;
            }

            System.out.println("Score: human " + humanScore + ", computer " + computerScore);

            String continuePlaying = readInput("Do you wish to continue playing? (y/n)?");

            if (continuePlaying.equals("n")) {
                System.out.println("Bye bye :)");
                break;
            } // else {
            // System.out.println("I do not understand " + continuePlaying + ". Could you try again?");
            roundCounter ++;
        }
    }


    /**
     * Reads input from console with given prompt
     *
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }
}
